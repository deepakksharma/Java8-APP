import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * New thing, sorting with using List.sort method but with creating our own
 * comparator
 */
public class ComparatorSorting {
	public static void main(String[] args) {
		List<Student> student = new ArrayList<>();
		student.add(new Student("Daam", 123654, 20));
		student.add(new Student("Maad", 741258, 89));
		student.add(new Student("Adam", 745896, 80));

		System.out.println("Unsorted");
		System.out.println("********");
		student.forEach(System.out::println);

		//	By using direct comparator and passing argument
		Collections.sort(student, new sortByName());
		System.out.println("");
		System.out.println("Sorted By Name");
		System.out.println("**************");
		student.forEach(System.out::println);

		//	By using anonymous function
		Collections.sort(student, new Comparator<Student>() {
			public int compare(Student r1, Student r2) {
				return r1.getRollno() - r2.getRollno();
			}
		});
		System.out.println("");
		System.out.println("Sorted By Roll Number");
		System.out.println("*********************");
		student.forEach(System.out::println);	
		
		// By using lambda
		student.sort((Student s1, Student s2) -> s1.getRollno() - s1.getRollno());
		System.out.println("");
		System.out.println("Sorted By Age");
		System.out.println("*************");
		student.forEach(System.out::println);			
	}

	public static class sortByName implements Comparator<Student> {

		@Override
		public int compare(Student o1, Student o2) {
			return o1.getName().compareTo(o2.getName());
		}

	}
}
